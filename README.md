# Geekstuff.dev / Devcontainers / Features / Terraform

This devcontainer feature installs and configures Terraform, using the latest
or a specific TF version, and its autocomplete.

It does this by setting up tfswitch which makes it easy at build time
and run time, to switch to any TF versions.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/basics": {},
        "ghcr.io/geekstuff-dev/devcontainers-features/terraform": {
            "tfswitch_version": "0.13.1300",
            "version": "latest"
        }
    }
}
```

You can use any debian, ubuntu or alpine image as the base.

This feature requires the basics feature since it needs the non-root user.

The above will pull use latest version of that feature, otherwise with **an example**
`v1.2.3` tag in this project source code, you would be able to use tags such as:

- `example.registry/some/path/feature:1`
- `example.registry/some/path/feature:1.2`
- `example.registry/some/path/feature:1.2.3`
- `example.registry/some/path/feature:latest`

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/starship/-/tags).
